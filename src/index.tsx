import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { StoreProvider } from './Store'
import HomePage from './HomePage'
import FavPage from './FavPage'
import { Router, RouteComponentProps } from '@reach/router'

const RouterPage = (props: { pageComponent: JSX.Element } & RouteComponentProps) => props.pageComponent

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider>
      <Router>
        <App path="/">
          <RouterPage pageComponent={<HomePage />} path="/"></RouterPage>
          <RouterPage pageComponent={<FavPage />} path="/faves"></RouterPage>
        </App>
      </Router>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
